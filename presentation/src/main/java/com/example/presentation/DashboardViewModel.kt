package com.example.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.data.Sport
import com.example.domain.interactors.Resource
import com.example.domain.usecases.DashboardUseCase

class DashboardViewModel(private val dashboardUseCase: DashboardUseCase) : ViewModel() {

    var sportsList: LiveData<Resource<List<Sport>>> = MutableLiveData()

    fun getSports() {
        sportsList = dashboardUseCase.getSports()
    }
}