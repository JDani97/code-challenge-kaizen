package com.example.domain.usecases

import androidx.lifecycle.LiveData
import com.example.domain.data.Sport
import com.example.domain.interactors.Resource
import com.example.domain.interactors.implementations.GetSportsFunction
import com.example.domain.repository.DashboardRepository

class DashboardUseCase(repository: DashboardRepository) {

    private val getSportsFunction: GetSportsFunction = GetSportsFunction(repository)

    fun getSports(): LiveData<Resource<List<Sport>>> {
        return getSportsFunction.get(Any())
    }
}
