package com.example.domain.interactors.implementations

import com.example.domain.data.Sport
import com.example.domain.interactors.LiveDataFunction
import com.example.domain.mapper.implementations.SportsResponseMapper
import com.example.domain.repository.DashboardRepository

class GetSportsFunction(repository: DashboardRepository) : LiveDataFunction<Any,List<Sport>,DashboardRepository>(repository) {

    private val mapper: SportsResponseMapper = SportsResponseMapper()

    override suspend fun run(params: Any): List<Sport> {
        return mapper.parseToView(repository.getSports())
    }
}
