package com.example.domain.interactors

data class Resource<out T>(val status: ResourceState, val data: T?, val error: Throwable?) {
    enum class ResourceState {
        LOADING,
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(ResourceState.SUCCESS, data, null)
        }

        fun <T> error(exception: Throwable): Resource<T> {
            return Resource(ResourceState.ERROR, null, exception)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(ResourceState.LOADING, data, null)
        }
    }
}
