package com.example.domain.interactors

abstract class AbstractFunction<in P, T, R : Any>(val repository: R) {
    protected abstract suspend fun run(params: P): T
}
