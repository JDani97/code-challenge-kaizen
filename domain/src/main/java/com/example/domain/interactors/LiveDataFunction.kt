package com.example.domain.interactors

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData

abstract class LiveDataFunction<in P, T, R : Any>(repository: R) : AbstractFunction<P, T, R>(repository) {

    fun get(params: P): LiveData<Resource<T>> {
        return liveData {
            emit(Resource.loading())
            try {
                val result = run(params)
                emit(Resource.success(result))
            } catch (ex: Throwable) {
                emit(Resource.error(ex))
            }
        }
    }
}
