package com.example.domain.data

data class Sport(val id: String, val name: String, val events: List<Event>)
