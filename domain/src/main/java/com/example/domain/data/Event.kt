package com.example.domain.data

data class Event(val eventId: String, val sportId: String, val firstName: String, val secondName: String, val startTime: Long, var isFavorite: Boolean)
