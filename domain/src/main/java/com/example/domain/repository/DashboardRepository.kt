package com.example.domain.repository

import com.example.repository.entities.RemoteSport
import com.example.repository.remote.DashboardRemote

class DashboardRepository(private val remote: DashboardRemote) {

    suspend fun getSports(): List<RemoteSport> {
        return remote.getSports()
    }
}
