package com.example.domain.mapper

abstract class AbstractRepositoryMapper<E, D> {
    abstract fun parseToRepository(data: D): E
}
