package com.example.domain.mapper

abstract class AbstractViewMapper<E, D> {
    abstract fun parseToView(entity: E): D
}
