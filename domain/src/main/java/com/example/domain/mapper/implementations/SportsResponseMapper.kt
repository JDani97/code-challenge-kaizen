package com.example.domain.mapper.implementations

import com.example.domain.data.Event
import com.example.domain.data.Sport
import com.example.domain.mapper.AbstractViewMapper
import com.example.repository.entities.RemoteSport

class SportsResponseMapper : AbstractViewMapper<List<RemoteSport>, List<Sport>>() {

    override fun parseToView(entity: List<RemoteSport>): List<Sport> {
        return entity.map { remoteSport ->
            Sport(
                id = remoteSport.id,
                name = remoteSport.name,
                events = remoteSport.events.map {
                    it.name.split(" - ").let { names ->
                        Event(it.id, it.sportId, names[0], names[1], it.startTime, false)
                    }
                }
            )
        }
    }
}
