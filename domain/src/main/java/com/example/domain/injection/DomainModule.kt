package com.example.domain.injection

import com.example.domain.repository.DashboardRepository
import com.example.domain.usecases.DashboardUseCase
import org.koin.dsl.module

val domainModule = module {
    factory<DashboardRepository> {
        DashboardRepository(get())
    }
    factory<DashboardUseCase> {
        DashboardUseCase(get())
    }
}
