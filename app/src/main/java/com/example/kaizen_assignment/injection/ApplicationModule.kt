package com.example.kaizen_assignment.injection

import com.example.domain.injection.domainModule
import com.example.presentation.injection.presentationModule
import com.example.repository.injection.repositoryModule
import org.koin.dsl.module

val applicationModule = module {

}

val appModules = listOf(applicationModule, presentationModule, domainModule, repositoryModule)