package com.example.kaizen_assignment.ui.base.recyclerview

import android.view.ViewGroup

interface ViewHolderTypeFactory {

    fun create(parent: ViewGroup, viewType: Int): BaseViewHolder<*>

    fun <T> type(model: T): Int
}