package com.example.kaizen_assignment.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.kaizen_assignment.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(root)
        }
    }
}