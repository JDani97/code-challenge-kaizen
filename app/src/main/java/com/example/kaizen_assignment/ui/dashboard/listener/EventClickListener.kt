package com.example.kaizen_assignment.ui.dashboard.listener

import com.example.kaizen_assignment.ui.base.recyclerview.ItemClickListener

interface EventClickListener: ItemClickListener {

    fun onFavoriteClicked(sportID: String, eventID: String)
}