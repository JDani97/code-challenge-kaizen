package com.example.kaizen_assignment.ui.base.recyclerview

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<in Model>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(model: Model, clickListener: ItemClickListener)
}