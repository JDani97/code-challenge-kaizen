package com.example.kaizen_assignment.ui.base.recyclerview

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ListAdapter(
    private var data: List<ItemType>,
    private val viewHolderTypeFactory: ViewHolderTypeFactory,
    private val clickListener: ItemClickListener
) : RecyclerView.Adapter<BaseViewHolder<ItemType>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<ItemType> {
        return viewHolderTypeFactory.create(
            parent, viewType
        ) as BaseViewHolder<ItemType>
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ItemType>, position: Int) {
        holder.bind(data[position], clickListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].viewType(viewHolderTypeFactory)
    }

    fun setData(itemList: ArrayList<ItemType>) {
        data = itemList
    }

}