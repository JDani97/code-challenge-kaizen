package com.example.kaizen_assignment.ui.dashboard.viewholder

import androidx.core.view.isVisible
import com.example.kaizen_assignment.R
import com.example.kaizen_assignment.databinding.ItemSportBinding
import com.example.kaizen_assignment.ui.base.recyclerview.BaseViewHolder
import com.example.kaizen_assignment.ui.base.recyclerview.ItemClickListener
import com.example.kaizen_assignment.ui.base.recyclerview.ListAdapter
import com.example.kaizen_assignment.ui.base.recyclerview.ViewHolderTypeFactoryImpl
import com.example.kaizen_assignment.ui.dashboard.model.EventItem
import com.example.kaizen_assignment.ui.dashboard.model.SportItem

class SportViewHolder(val binding: ItemSportBinding) : BaseViewHolder<SportItem>(binding.root) {

    companion object {
        const val layout = R.layout.item_sport
    }

    override fun bind(model: SportItem, clickListener: ItemClickListener) {
        val viewHolderTypeFactory: ViewHolderTypeFactoryImpl = ViewHolderTypeFactoryImpl()
        with(binding) {
            model.apply {
                tvSportName.text = sportTitle
                ivSportIcon.apply {
                    setOnClickListener {
                        isSelected = !isSelected
                        rvSportEvents.isVisible = !rvSportEvents.isVisible
                    }
                }
                rvSportEvents.adapter = ListAdapter(
                    eventList.map { it as EventItem }.sortedBy { !it.event.isFavorite },
                    viewHolderTypeFactory,
                    clickListener
                )
            }
        }
    }
}
