package com.example.kaizen_assignment.ui.dashboard.model

import com.example.kaizen_assignment.ui.base.recyclerview.ItemType
import com.example.kaizen_assignment.ui.base.recyclerview.ViewHolderTypeFactory

class SportItem(val sportID: String, val sportTitle: String, var eventList: ArrayList<ItemType>) : ItemType {
    override fun viewType(viewHolderTypeFactory: ViewHolderTypeFactory): Int {
        return viewHolderTypeFactory.type(this)
    }
}
