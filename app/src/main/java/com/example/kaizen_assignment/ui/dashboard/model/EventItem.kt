package com.example.kaizen_assignment.ui.dashboard.model

import com.example.domain.data.Event
import com.example.kaizen_assignment.ui.base.recyclerview.ItemType
import com.example.kaizen_assignment.ui.base.recyclerview.ViewHolderTypeFactory

class EventItem(val event: Event) : ItemType {
    override fun viewType(viewHolderTypeFactory: ViewHolderTypeFactory): Int {
        return viewHolderTypeFactory.type(this)
    }
}