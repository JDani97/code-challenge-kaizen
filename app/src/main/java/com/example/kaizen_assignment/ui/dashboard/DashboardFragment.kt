package com.example.kaizen_assignment.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.domain.data.Sport
import com.example.domain.interactors.Resource.ResourceState
import com.example.domain.interactors.Resource
import com.example.kaizen_assignment.R
import com.example.kaizen_assignment.databinding.FragmentDashboardBinding
import com.example.kaizen_assignment.ui.base.recyclerview.ItemType
import com.example.kaizen_assignment.ui.base.recyclerview.ListAdapter
import com.example.kaizen_assignment.ui.base.recyclerview.ViewHolderTypeFactoryImpl
import com.example.kaizen_assignment.ui.dashboard.listener.EventClickListener
import com.example.kaizen_assignment.ui.dashboard.model.EventItem
import com.example.kaizen_assignment.ui.dashboard.model.SportItem
import com.example.presentation.DashboardViewModel
import okhttp3.internal.notify
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DashboardFragment : Fragment(), EventClickListener {

    private var binding: FragmentDashboardBinding? = null
    private val dashboardViewModel: DashboardViewModel by sharedViewModel()
    private val adapter: ListAdapter = ListAdapter(listOf(), ViewHolderTypeFactoryImpl(), this)
    private val itemList: ArrayList<ItemType> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDashboardBinding.bind(view).apply {
            binding?.rvDashboard?.adapter = adapter
        }
        dashboardViewModel.getSports()
    }

    override fun onStart() {
        super.onStart()
        dashboardViewModel.sportsList.observe(this, sportsListObserver)
    }

    override fun onStop() {
        super.onStop()
        dashboardViewModel.sportsList.removeObservers(this)
    }

    private val sportsListObserver: Observer<Resource<List<Sport>>> = Observer { resource ->
        when (resource.status) {
            ResourceState.SUCCESS -> {
                setUpRecyclerView()
            }
            ResourceState.ERROR -> {
                //  TODO - Handle API error here
            }
            ResourceState.LOADING -> {
                //  TODO - Handle Loading animation here
            }
        }
    }

    private fun setUpRecyclerView() {
        dashboardViewModel.sportsList.value?.data?.let {
            binding?.rvDashboard?.adapter = adapter
            itemList.clear()
            itemList.addAll(ArrayList(it.map { sport -> SportItem(sport.id, sport.name, ArrayList(sport.events.map { event -> EventItem(event) })) }))
            adapter.setData(itemList)
        }
    }

    override fun onFavoriteClicked(sportID: String, eventID: String) {
        itemList.map { it as SportItem }.first { it.sportID == sportID }.eventList.map { (it as EventItem).event }.first { it.eventId == eventID }
            .apply {
                isFavorite = !isFavorite
            }
        adapter.setData(itemList)
        adapter.notifyDataSetChanged()
    }
}
