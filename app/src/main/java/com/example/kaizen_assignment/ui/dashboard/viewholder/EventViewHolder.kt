package com.example.kaizen_assignment.ui.dashboard.viewholder

import android.os.CountDownTimer
import com.example.kaizen_assignment.R
import com.example.kaizen_assignment.databinding.ItemEventBinding
import com.example.kaizen_assignment.ui.base.recyclerview.BaseViewHolder
import com.example.kaizen_assignment.ui.base.recyclerview.ItemClickListener
import com.example.kaizen_assignment.ui.dashboard.listener.EventClickListener
import com.example.kaizen_assignment.ui.dashboard.model.EventItem
import com.example.kaizen_assignment.ui.extentions.millisToHHMMSS

class EventViewHolder(val binding: ItemEventBinding) : BaseViewHolder<EventItem>(binding.root) {

    companion object {
        const val layout = R.layout.item_event
    }

    private var countDownTimer: CountDownTimer? = null

    override fun bind(model: EventItem, clickListener: ItemClickListener) {
        with(binding) {
            model.event.apply {
                countDownTimer?.cancel()
                countDownTimer = object : CountDownTimer(startTime, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        tvEventTimer.text = millisUntilFinished.millisToHHMMSS()
                    }
                    override fun onFinish() {
                        TODO("Handle UI when timer reaches 0")
                    }
                }.start()
                tvEventFirstName.text = firstName
                tvEventSecondName.text = secondName
                ivEventFavourite.apply {
                    isSelected = isFavorite
                    setOnClickListener { (clickListener as EventClickListener).onFavoriteClicked(sportId, eventId) }
                }
            }
        }
    }


}