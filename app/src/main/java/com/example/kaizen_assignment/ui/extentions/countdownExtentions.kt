package com.example.kaizen_assignment.ui.extentions

import java.util.concurrent.TimeUnit

fun Long.millisToHHMMSS(): String{
    return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(this),
        TimeUnit.MILLISECONDS.toMinutes(this) % TimeUnit.HOURS.toMinutes(1),
        TimeUnit.MILLISECONDS.toSeconds(this) % TimeUnit.MINUTES.toSeconds(1));
}