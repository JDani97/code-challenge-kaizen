package com.example.kaizen_assignment.ui.base.recyclerview

interface ItemType {
    fun viewType(viewHolderTypeFactory: ViewHolderTypeFactory): Int
}