package com.example.kaizen_assignment.ui.base.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.kaizen_assignment.databinding.ItemEventBinding
import com.example.kaizen_assignment.databinding.ItemSportBinding
import com.example.kaizen_assignment.ui.dashboard.model.EventItem
import com.example.kaizen_assignment.ui.dashboard.model.SportItem
import com.example.kaizen_assignment.ui.dashboard.viewholder.EventViewHolder
import com.example.kaizen_assignment.ui.dashboard.viewholder.SportViewHolder

class ViewHolderTypeFactoryImpl : ViewHolderTypeFactory {

    override fun <T> type(model: T): Int {
        return when (model) {
            is EventItem -> EventViewHolder.layout
            is SportItem -> SportViewHolder.layout
            else -> EventViewHolder.layout
        }
    }

    override fun create(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            EventViewHolder.layout -> {
                val itemBinding = ItemEventBinding.inflate(inflater, parent, false)
                EventViewHolder(itemBinding)
            }
            SportViewHolder.layout -> {
                val itemBinding = ItemSportBinding.inflate(inflater, parent, false)
                SportViewHolder(itemBinding)
            }
            else -> throw UnsupportedClassVersionError("ViewHolder not found")
        }
    }
}