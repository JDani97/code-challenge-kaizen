package com.example.kaizen_assignment.ui.base.recyclerview

interface ItemClickListener {
    fun onItemClicked(id: String) {}
}