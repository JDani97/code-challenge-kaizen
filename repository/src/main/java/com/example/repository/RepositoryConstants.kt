package com.example.repository

class RepositoryConstants {
    companion object {
        const val CALL_TIMEOUT_IN_SECONDS = 30L
    }
}