package com.example.repository.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteSport(
    @SerialName("i") val id: String,
    @SerialName("d") val name: String,
    @SerialName("e") val events: List<RemoteEvent>
)
