package com.example.repository.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteEvent(
    @SerialName("i") val id: String,
    @SerialName("si") val sportId: String,
    @SerialName("d") val name: String,
    @SerialName("tt") val startTime: Long
)
