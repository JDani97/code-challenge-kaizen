package com.example.repository.remote.implementation

import kotlinx.coroutines.CompletableDeferred
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response

abstract class RemoteImpl {

    protected fun <T> getCallback(deferred: CompletableDeferred<T>): Callback<T> {
        return object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                response.body()?.let {
                    deferred.complete(it)
                } ?: run {
                    deferred.completeExceptionally(HttpException(response))
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                deferred.completeExceptionally(t)
            }
        }
    }
}
