package com.example.repository.remote

import com.example.repository.entities.RemoteSport

interface DashboardRemote {

    suspend fun getSports(): List<RemoteSport>
}
