package com.example.repository.remote.service

import com.example.repository.entities.RemoteSport
import retrofit2.Call
import retrofit2.http.GET

interface DashboardService {

    @GET("sports")
    fun getSports(): Call<List<RemoteSport>>
}
