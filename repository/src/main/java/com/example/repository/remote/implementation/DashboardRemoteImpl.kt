package com.example.repository.remote.implementation

import com.example.repository.entities.RemoteSport
import com.example.repository.remote.DashboardRemote
import com.example.repository.remote.service.DashboardService
import kotlinx.coroutines.CompletableDeferred

class DashboardRemoteImpl(
    private val dashboardService: DashboardService
) : DashboardRemote, RemoteImpl() {

    override suspend fun getSports(): List<RemoteSport> {
        val deferred = CompletableDeferred<List<RemoteSport>>()
        dashboardService.getSports().enqueue(getCallback(deferred))
        return deferred.await()
    }
}