package com.example.repository.injection

import com.example.repository.RepositoryConstants
import com.example.repository.remote.DashboardRemote
import com.example.repository.remote.implementation.DashboardRemoteImpl
import com.example.repository.remote.service.DashboardService
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

const val DASHBOARD_SERVICE_NAME = "dashboard"
val JSON_MEDIA_TYPE = "application/json".toMediaType()

val repositoryModule = module {

    single<Converter.Factory> {
        Json { ignoreUnknownKeys = true }.asConverterFactory(JSON_MEDIA_TYPE)
    }

    single<Retrofit>(named(DASHBOARD_SERVICE_NAME)) {
        Retrofit.Builder()
            .baseUrl("https://618d3aa7fe09aa001744060a.mockapi.io/api/")
            .addConverterFactory(get())
            .build()
    }

    single<DashboardRemote> {
        val dashboardService = get<Retrofit>(named(DASHBOARD_SERVICE_NAME)).create(DashboardService::class.java)
        DashboardRemoteImpl(dashboardService)
    }

}